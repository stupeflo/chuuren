describe Chuuren::Models::League do
  def new_league(data)
    Chuuren::Models::League::new data
  end
  subject { new_league name: 'Regular League',
                       description: '4 player game, Chuuren Potos Rules',
                       game_type: :regular }

  it { is_expected.to have_attributes name: 'Regular League',
                                      description: '4 player game, Chuuren Potos Rules',
                                      game_type: :regular,
                                      id: nil }
  it ("should be converted to string") { is_expected.to satisfy { |player| "#{player}" == 'Regular League' } }
  context "comparission" do
    it ("should be eq with another player called Regular League") { is_expected.to eq new_league(name: 'Regular League') }
    it ("should not be eq with another player called San-Nin League") { is_expected.not_to eq new_league(name: 'San-Nin League') }
  end
end
