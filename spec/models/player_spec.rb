describe Chuuren::Models::Player do
  def new_player(name)
    Chuuren::Models::Player::new name: name
  end
  subject { new_player('Akagi') }

  it { is_expected.to have_attributes name: 'Akagi', id: nil }
  it ("should be converted to string") { is_expected.to satisfy { |player| "#{player}" == 'Akagi' } }
  context "comparission" do
    it ("should be eq with another player called Akagi") { is_expected.to eq new_player ('Akagi') }
    it ("should not be eq with another player called Washizu") { is_expected.not_to eq new_player ('Washizu') }
  end
end
