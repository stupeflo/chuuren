module Chuuren
  module Storage
    class PlayerStorage < Base
      include Crud
      include Filter

      table :player
      fields [:name]
      search_fields [:name]
      model_type Player
    end
  end
end