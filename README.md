# Chuuren Potos League App

A Web tool to manage mah-jong Leagues over time. Provides Rest API and Web interface

[![build status](https://git.framasoft.org/ci/projects/67/status.png?ref=master)](https://git.framasoft.org/floreal/chuuren/builds?scope=finished)

# Features

- :ballot_box_with_check: Registering players to a game
- :ballot_box_with_check: Registering Scores
- :ballot_box_with_check: Regular games.
- :ballot_box_with_check: San-nin Games.
- :ballot_box_with_check: Uma management
- Player History
- Persistence
- Web interface
- Web API

# Licence

This piece of software can be studied, distributed, shared and used
according to [BSD Licence](LICENSE).