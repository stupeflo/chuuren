require 'cucumber/rake/task'
require 'rspec/core/rake_task'
require 'sqlite3'
require 'ci/reporter/rake/rspec'


db_uri = 'db/test.sqlite3'

namespace :db do
  desc 'Creates database'
  task :setup do
    db = SQLite3::Database::new(db_uri)
    tables = db.execute('SELECT name FROM sqlite_master WHERE type=?;', ['table']).to_a.flatten

    db.execute(<<-SQL
        CREATE TABLE player (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          name VARCHAR(255) NOT NULL UNIQUE
        )
    SQL
    ) unless tables.include?('player')

    db.execute(<<-SQL
        CREATE TABLE league (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          name VARCHAR(255) NOT NULL UNIQUE,
          description TEXT NULL,
          game_type INTEGER NOT NULL DEFAULT 0
        )
    SQL
    ) unless tables.include?('league')

    db.close
  end

  desc 'Destroys database'
  task :cleanup do
    rm_f db_uri
  end
end

namespace :ci do
  desc 'Prepares workspace'
  task :prepare => ['db:setup', 'ci:setup:rspecdoc'] do
    mkdir_p 'features/reports'
  end

  desc 'Cleans up workspace'
  task :cleanup => "db:cleanup" do
    rm_rf 'spec/reports'
    rm_rf 'features/reports'
    rm_rf 'coverage'
  end

  RSpec::Core::RakeTask.new(:spec => :prepare)

  Cucumber::Rake::Task.new(:features => :prepare) do |task|
    task.cucumber_opts = '-c -fjson -o features/reports/cucumber.json -fpretty'
  end
end

desc 'executes full Continuous Integration workflow'
task :ci => ["ci:features", "ci:spec"]

RSpec::Core::RakeTask.new(:spec => "db:setup") do |task|
  task.rspec_opts = "-fd -c"
end

Cucumber::Rake::Task.new(:features => "db:setup") do |task|
  task.cucumber_opts = '-c'
end